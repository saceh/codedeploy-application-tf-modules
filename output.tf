output "codedeploy_app_name" {
  value = "${aws_codedeploy_app.main.name}"
}

output "codedeploy_app_id" {
  value = "${aws_codedeploy_app.main.id}"
}

